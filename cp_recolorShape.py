import maya.cmds as mc
import maya.OpenMaya as om


def recolor_shape(inputList= None, inputColor= 'yellow'):
    """ recolors nurbs curve shape nodes based on input
    
    :param1 inputList: objects to iterate over // if None: function operates on selection
    :type1 inputList: None // list // string
    
    :param2 inputColor: selects the color to be changed to
    :type2 inputColor: string (yellow / blue / red) // integer
        
    :return feedBackList:
    :rtype feedBackList: list
    
    """
    
    
    ################################################################################################################## Color Check
    
    # check if inputColor is string
    if type(inputColor) == str:
        
        newInputColor = inputColor.upper()
        
        colorDict= {'YELLOW': 22, 'BLUE': 18, 'RED': 20}
        
        if newInputColor in colorDict:
            colorValue= colorDict[newInputColor]
        
        else:
            om.MGlobal.displayInfo('{color} is not in the color dictionary, default (yellow // 22) is used'.format(color= inputColor))
            colorValue= 22
    
    # check if inputColor is int
    elif type(inputColor) == int:
        
        # check if inputColor is in Range
        if inputColor <= 31 and inputColor >= 0:
            colorValue= inputColor
        
        else:
            om.MGlobal.displayInfo('{color} is out of index range, default (yellow // 22) is used'.format(color= str(inputColor)))
            colorValue= 22
        
    else:
        om.MGlobal.displayInfo('{color} is neither a string nor an integer, default (yellow // 22) is used'.format(color= str(inputColor)))
        colorValue= 22
    
    
    
    
    ################################################################################################################## Selection Check
    
    # check if inputList is Emtpy
    if not inputList:
        defaultSelectionList= mc.ls(sl=True)
    
    else:
        defaultSelectionList= []
        
        # check if inputList is string
        if type(inputList) == str:
            defaultSelectionList.append(inputList)
        
        # check if inputList is list
        elif type(inputList) == list:
            if len(inputList) >= 1:
                for item in inputList:
                    if isinstance(item, str):
                        defaultSelectionList.append(item)
                    
                    elif isinstance(item, list):
                        om.MGlobal.displayInfo('{item} is a list in a list, we will continue without it'.format(item= str(item)))
                    
                    else:
                        om.MGlobal.displayInfo('{item} is not of a valid type (string) to be in your list, we will continue without it'.format(item= str(item)))
        
        else:
            om.MGlobal.displayInfo('please use a valid type (string/list)')
    
    # check if resultList is valid
    if defaultSelectionList:
        
        feedBackList= []
        validChildrenList= []
        
        # loop over resultList
        for selItem in defaultSelectionList:
            
            # check if object is a shape
            if mc.objectType(selItem, isType= 'nurbsCurve'):
                transformNode= mc.listRelatives(selItem, parent= True)[0]
                validChildrenList.append(selItem)
                
            selChildrenList= mc.listRelatives(selItem, shapes= True)

            # check if object has a shape
            if selChildrenList:
                
                # loop over shapeList
                for child in selChildrenList:
                    typeCheck= mc.objectType(child, isType= 'nurbsCurve')
                    
                    # check if shape is nurbsCurve
                    if typeCheck:
                        validChildrenList.append(child)
                    
                    else:
                        om.MGlobal.displayInfo('{node} is not a nurbsCurve'.format(node= child))
                        
        if validChildrenList:
            for valChild in validChildrenList:
                mc.setAttr('{node}.overrideEnabled'.format(node= valChild), 1)
                mc.setAttr('{node}.overrideColor'.format(node= valChild), colorValue)
            
                feedBackList.append(valChild)
                
        else:
            om.MGlobal.displayInfo('{node} has no valid shape nodes'.format(node= selItem))

    
    if feedBackList:
        feedBackListLen= len(feedBackList)
        feedBackString= '\n           '.join(feedBackList)
        print ('\n##########################################################\n\nfrom the input list {length} items have been changed \nthey were: {listString}\n\n##########################################################\n'.format(length= feedBackListLen, listString= feedBackString))
        om.MGlobal.displayInfo('operation finished on {listString}'.format(listString= feedBackString))
        
        return feedBackList
    
    
    else:
        om.MGlobal.displayInfo('there was no valid selection made')
        
        return None
